// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

//Given test cases
TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}


//Edge cases: i > j

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 900)), make_tuple(1000, 900, 174));
}

//Various edge cases:

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 1000)), make_tuple(1000, 1000, 112));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(999, 1000)), make_tuple(999, 1000, 112));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(2000, 3000)), make_tuple(2000, 3000, 217));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(2000, 3001)), make_tuple(2000, 3001, 217));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(500, 2500)), make_tuple(500, 2500, 209));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 1999)), make_tuple(1000, 1999, 182));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(1999, 3999)), make_tuple(1999, 3999, 238));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(501, 500)), make_tuple(501, 500, 111));
}

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1000)), make_tuple(1, 1000, 179));
}

TEST(CollatzFixture, eval15) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 4000)), make_tuple(1000, 4000, 238));
}

TEST(CollatzFixture, eval16) {
    ASSERT_EQ(collatz_eval(make_pair(999, 1999)), make_tuple(999, 1999, 182));
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
